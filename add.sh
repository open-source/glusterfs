#!/bin/bash

SLAVE=$1;
MASTER=$2;
REPLICA=${3:-2};

MIN_REPLICA=$((REPLICA-1));

docker-compose exec $MASTER bash -c "echo 'y' | gluster volume remove-brick monpartage replica $MIN_REPLICA $SLAVE:/data/glusterfs/vol0/brick0 force"
docker-compose exec $MASTER bash -c "echo 'y' | gluster peer detach $SLAVE"
docker-compose exec $MASTER gluster peer probe $SLAVE
docker-compose exec $MASTER gluster volume add-brick monpartage replica $REPLICA $SLAVE:/data/glusterfs/vol0/brick0 force
docker-compose exec $MASTER gluster volume heal monpartage full


# sudo gluster volume set volume1 auth.allow <ip>; \
