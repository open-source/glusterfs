#!/bin/bash

echo 'stor0 :';
docker-compose exec stor0 ls -l;
echo

echo 'stor1 :';
docker-compose exec stor1 ls -l;
echo

echo 'client1 :';
docker-compose exec glusterclient1 ls -l;
echo

echo 'client2 :';
docker-compose exec glusterclient2 ls -l;
echo


